﻿//**************************************************
// CameraController.cs
//
// Code Soldiers 2020
//
// Author: Rafał Kania
// Creation Date: 13 june 2020
//**************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CodeSoldiers
{
	public class CameraController : MonoBehaviour
	{
		//Singleton
		public static CameraController Instance;

        [SerializeField]
        new Camera camera = null;
        public Camera _Camera
        {
            get
            {
                return camera;
            }
        }

        [SerializeField]
        Vector3 cameraOriginalPosition;

        [SerializeField]
        float cameraOffset = 0;

        [SerializeField]
        float shakeAmount = 0.7f;

        [SerializeField]
        bool shouldShake = false;

        [SerializeField]
        MainObject mainObject;
        public MainObject _MainObject
        {
            get
            {
                return mainObject;
            }

            set
            {
                mainObject = value;
            }
        }

        void Awake()
		{
			if (Instance != null)
				Destroy(gameObject);
			else
				Instance = this;
		}
		
		void Start()
		{
            mainObject = MainObject.Instance;

            cameraOriginalPosition = camera.transform.localPosition;
        }

        void Update()
        {
            if (shouldShake)
            {
                camera.transform.localPosition = cameraOriginalPosition + Random.insideUnitSphere * shakeAmount;
            }

            if (mainObject != null)
            {
                Vector3 tempTransform = transform.position;

                tempTransform = mainObject.transform.position;
                tempTransform.y += cameraOffset;

                transform.position = tempTransform;
            }

        }

        public IEnumerator CameraShaker()
        {
            shouldShake = true;
            yield return new WaitForSeconds(0.2f);
            shouldShake = false;
            camera.transform.localPosition = cameraOriginalPosition;
        }

        public void ShakeCamera()
        {
            StartCoroutine(CameraShaker());
        }
    }
}
