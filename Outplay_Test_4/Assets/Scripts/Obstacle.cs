﻿//**************************************************
// Obstacle.cs
//
// Code Soldiers 2020
//
// Author: Rafał Kania
// Creation Date: 13 June 2020
//**************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CodeSoldiers
{
    public enum ObstacleType
    {
        Obstacle = 0,
        DestinationPoint
    }

	public class Obstacle : MonoBehaviour
	{

        [SerializeField]
        private ObstacleType currentObstacleType = ObstacleType.Obstacle;
        public ObstacleType _CurrentObstacleType
        {
            get
            {
                return currentObstacleType;
            }

            set
            {
                currentObstacleType = value;
            }
        }

        [SerializeField]
        private MeshRenderer meshRenderer = null;


        void Awake()
		{

		}
		
		void Start()
		{
            if (meshRenderer == null)
                meshRenderer = GetComponent<MeshRenderer>();

            switch (currentObstacleType)
            {
                case ObstacleType.Obstacle:
                    meshRenderer.materials[0].SetColor("_Color", Color.white);
                    break;

                case ObstacleType.DestinationPoint:
                    meshRenderer.materials[0].SetColor("_Color", Color.yellow);
                    break;
            }
		}
		
		void Update()
		{
			
		}
	}
}
