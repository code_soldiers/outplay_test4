﻿//**************************************************
// MainObject.cs
//
// Code Soldiers 2020
//
// Author: Rafał Kania
// Creation Date: 13 June 2020
//**************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CodeSoldiers
{
	public class MainObject : MonoBehaviour
	{
		//Singleton
		public static MainObject Instance;

        [SerializeField]
        Transform currentDestinationPoint = null;

        [SerializeField]
        Vector3 startPosition = Vector3.zero;

        [SerializeField]
        private bool isMoving = false;

        [SerializeField]
        private List<Obstacle> destinationPoints = new List<Obstacle>();

        [SerializeField]
        float velocity = 0f;

        [SerializeField]
        private new Rigidbody rigidbody = null;

        [SerializeField]
        private AudioSource audioSource = null;

        [SerializeField]
        private MeshRenderer meshRenderer = null;

        [SerializeField]
        private List<AudioClip> audioClips = new List<AudioClip>();

        [SerializeField]
        ParticleSystem particles = null;
		
		void Awake()
		{
			if (Instance != null)
				Destroy(gameObject);
			else
				Instance = this;
		}
		
		void Start()
		{
            if (rigidbody == null)
                rigidbody = GetComponent<Rigidbody>();

            if (audioSource == null)
                audioSource = GetComponent<AudioSource>();

            if (meshRenderer == null)
                meshRenderer = GetComponent<MeshRenderer>();

            transform.position = startPosition;

            if ( ObstacleManager.Instance._Obstacles.Count > 0 && ObstacleManager.Instance.transform.childCount == ObstacleManager.Instance._Obstacles.Count)
            {

                for (int i = 0; i < ObstacleManager.Instance._Obstacles.Count; i++)
                {
                    if (ObstacleManager.Instance._Obstacles[i]._CurrentObstacleType == ObstacleType.DestinationPoint)
                    {
                        destinationPoints.Add(ObstacleManager.Instance._Obstacles[i]);
                    }
                }

                SetNewDestinationPoint();
            } 
		}
		
		void Update()
		{
            if (Input.GetKeyDown(KeyCode.Space))
            {
                if (isMoving == true)
                {
                    isMoving = false;
                }
                else
                {
                    isMoving = true;
                }
            }
		}

        private void FixedUpdate()
        {
            if (isMoving == true)
            {
                MoveObject();
            }
        }

        public void MoveObject()
        {
            var heading = currentDestinationPoint.position - transform.position;
            var distance = heading.magnitude;
            var direction = heading / distance;

            rigidbody.MovePosition(transform.position + (direction * velocity * Time.deltaTime));
        }

        private void SetNewDestinationPoint()
        {
            if(destinationPoints.Count > 0)
                currentDestinationPoint = destinationPoints[0].transform;
        }

        public void PlayAudio(AudioSource _audioSource, AudioClip _clip)
        {
            _audioSource.clip = _clip;
            _audioSource.Play();
        }

        private void Die()
        {
            StartCoroutine(CRDie());
        }

        private IEnumerator CRDie()
        {
            isMoving = false;
            meshRenderer.enabled = false;
            yield return new WaitForSeconds(0.1f);
            particles.Play();
            yield return new WaitForSeconds(2f);
            gameObject.SetActive(false);
            Destroy(gameObject);

        }

        private void OnTriggerEnter(Collider other)
        {
           
            if (other.gameObject.tag.Equals("Obstacle"))
            {
                var obstacle = other.gameObject.GetComponent<Obstacle>();

                switch (obstacle._CurrentObstacleType)
                {
                    case ObstacleType.Obstacle:
                        CameraController.Instance.ShakeCamera();

                        PlayAudio(audioSource, audioClips[0]);

                        Die();
                        Debug.Log(other.gameObject.name);
                        break;

                    case ObstacleType.DestinationPoint:
                        CameraController.Instance.ShakeCamera();

                        Debug.Log(other.gameObject.name);

                        isMoving = false;

                        if (destinationPoints.Count > 1)
                        {
                            destinationPoints.RemoveAt(0);

                            SetNewDestinationPoint();

                            isMoving = true;
                        }
                        else
                        {
                            PlayAudio(audioSource, audioClips[1]);

                            Die();
                        }

                        break;
                }
            }
        }
    }
}