﻿//**************************************************
// ObstacleManager.cs
//
// Code Soldiers 2020
//
// Author: Rafał Kania
// Creation Date: 13 June 2020
//**************************************************

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace CodeSoldiers
{
    [ExecuteInEditMode]
	public class ObstacleManager : MonoBehaviour
	{
		//Singleton
		public static ObstacleManager Instance;

        [SerializeField]
        float randomIndex = 100;

        [SerializeField]
        private Obstacle obstacleToSpawn = null;

        [SerializeField]
        private int objectsCount = 100;

        [SerializeField]
        private List<Obstacle> obstacles = new List<Obstacle>();
        public List<Obstacle> _Obstacles
        {
            get
            {
                return obstacles;
            }

            set
            {
                obstacles = value;
            }
        }

        void Awake()
		{
			if (Instance != null)
				Destroy(gameObject);
			else
				Instance = this;
		}
		
		void Start()
		{

        }
		
		void Update()
		{

        }

        [ContextMenu("Spawn Objects")]
        public void SpawnObstacles()
        {
            obstacles.Clear();

            if (transform.childCount == 0)
            {
                for (int i = 0; i < objectsCount; i++)
                {
                    var o = Instantiate(obstacleToSpawn) as Obstacle;
                    o.gameObject.name = string.Format("Obstacle{0}", i);

                    var ot = o._CurrentObstacleType = ObstacleType.Obstacle;

                    o.transform.parent = transform;

                    var y = 0;

                    o.transform.position = new Vector3(Random.Range(-randomIndex, randomIndex), y, Random.Range(-randomIndex, randomIndex));

                    obstacles.Add(o);

                    if (o.gameObject.activeSelf == false)
                        o.gameObject.SetActive(true);
                }
            }
        }
    }
}
